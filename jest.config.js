module.exports = {
  globals: {
    __DEV__: true,
  },
  setupFilesAfterEnv: ['./jest.setup.ts'],
  verbose: false, // false since we want to see console.logs inside tests
  bail: false,
  testURL: 'http://localhost/',
  testEnvironment: 'jsdom',
  testRegex: './tests/(?!.eslintrc).*.(j|t)s$',
  rootDir: '.',
  testPathIgnorePatterns: [
    '<rootDir>/components/coverage/',
    '<rootDir>/test/cypress/',
    '<rootDir>/test/coverage/',
    '<rootDir>/dist/',
    '<rootDir>/node_modules/',
  ],
  moduleFileExtensions: ['js', 'json', 'ts'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  resolver: null,
  transformIgnorePatterns: [
    'node_modules/core-js',
    'node_modules/babel-runtime',
    'node_modules/vue',
  ],
  transform: {
    '\\.ts$': 'ts-jest',
  },
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.{ts,js}'],
};
