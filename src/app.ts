import 'reflect-metadata';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as cors from 'cors';
import routes from './routes/index.route';

// database
import { createDatabaseConnexion, setRepositories } from './db';
// config
import config from './config/config';
import { Server } from 'http';

let server: Server;

const app = express();

// Server port
const HTTP_PORT = config.server.port;

// Call midlewares
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());

// Start server
createDatabaseConnexion()
  .then((connection) => {
    // set repositories
    setRepositories(connection);
    //Set all routes from routes folder
    server = app.listen(
      process.env.NODE_ENV !== 'test' ? HTTP_PORT : null,
      () => {
        // console.log(`Server running on port ${HTTP_PORT}`);
      }
    );
  })
  .catch((error) => {});

app.use('/', routes);

export default {
  getApp: () => app,
  server: server,
};
