import { Repository } from 'typeorm';
import { Challenge } from '../entities/challenge.entity';

export class ChallengeRepository {
  private static instance: ChallengeRepository;
  _challengeRepository: Repository<Challenge>;

  private constructor() {}

  static getInstance(connection?) {
    if (!ChallengeRepository.instance && connection) {
      ChallengeRepository.instance = new ChallengeRepository();
      ChallengeRepository.instance._challengeRepository = connection.getRepository(
        Challenge
      );
    }
    return ChallengeRepository.instance;
  }

  getChallengeRepository(): Repository<Challenge> {
    return this._challengeRepository;
  }
}
