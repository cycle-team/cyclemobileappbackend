import { getRepository, Repository } from 'typeorm';
import { Group } from '../entities/group.entity';

export class GroupRepository {
  private static instance: GroupRepository;
  _groupRepository: Repository<Group>;

  private constructor() {}

  static getInstance(connection?) {
    if (!GroupRepository.instance && connection) {
      GroupRepository.instance = new GroupRepository();
      GroupRepository.instance._groupRepository = getRepository(Group);
    }
    return GroupRepository.instance;
  }

  getGroupRepository(): Repository<Group> {
    return this._groupRepository;
  }
}
