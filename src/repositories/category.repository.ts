import { Repository } from 'typeorm';
import { Category } from '../entities/category.entity';

export class CategoryRepository {
  private static instance: CategoryRepository;
  _categoryRepository: Repository<Category>;

  private constructor() {}

  static getInstance(connection?) {
    if (!CategoryRepository.instance && connection) {
      CategoryRepository.instance = new CategoryRepository();
      CategoryRepository.instance._categoryRepository = connection.getRepository(
        Category
      );
    }
    return CategoryRepository.instance;
  }

  getCategoryRepository(): Repository<Category> {
    return this._categoryRepository;
  }
}
