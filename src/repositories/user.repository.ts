import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';

export class UserRepository {
  private static instance: UserRepository;
  _userRepository: Repository<User>;

  private constructor() {}

  static getInstance(connection?) {
    if (!UserRepository.instance && connection) {
      UserRepository.instance = new UserRepository();
      UserRepository.instance._userRepository = connection.getRepository(User);
    }
    return UserRepository.instance;
  }

  getUserRepository(): Repository<User> {
    return this._userRepository;
  }
}
