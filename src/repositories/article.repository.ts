import { getRepository, Repository } from 'typeorm';
import { Article } from '../entities/article.entity';

export class ArticleRepository {
  private static instance: ArticleRepository;
  _articleRepository: Repository<Article>;

  private constructor() {}

  static getInstance(connection?) {
    if (!ArticleRepository.instance && connection) {
      ArticleRepository.instance = new ArticleRepository();
      ArticleRepository.instance._articleRepository = getRepository(Article);
    }
    return ArticleRepository.instance;
  }

  getArticleRepository(): Repository<Article> {
    return this._articleRepository;
  }
}
