import { getRepository, Repository } from 'typeorm';
import { Tag } from '../entities/tag.entity';

export class TagRepository {
  private static instance: TagRepository;
  _tagRepository: Repository<Tag>;

  private constructor() {}

  static getInstance(connection?) {
    if (!TagRepository.instance && connection) {
      TagRepository.instance = new TagRepository();
      TagRepository.instance._tagRepository = getRepository(Tag);
    }
    return TagRepository.instance;
  }

  getTagRepository(): Repository<Tag> {
    return this._tagRepository;
  }
}
