import { Router } from 'express';
import ArticleController from '../controllers/article.controller';
import { checkJwt } from '../middlewares/checkJwt';
import { checkRole } from '../middlewares/checkRole';
import { ROLES } from "../utils/Roles.enum";

const router = Router();
const articleController = new ArticleController();

//Get Articles
router.get('/', [checkJwt, checkRole(['ADMIN', 'USER'])], articleController.get);

// Get one article  // TODO ([a-f0-9]+) add regex verification in path
router.get('/:id', [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])], articleController.getOneById);

//Create a new article
router.post('/', [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])], articleController.create);

//Edit one article
router.patch('/:id', [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])], articleController.edit);

//Delete one article
router.delete('/:id', [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])], articleController.remove);

export default router; 
