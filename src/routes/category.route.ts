import { Router } from 'express';
// Middlewares
import { checkJwt } from '../middlewares/checkJwt';
import { checkRole } from '../middlewares/checkRole';
// Personal Import
import { ROLES } from '../utils/Roles.enum';
import CategoryController from '../controllers/category.controller';

const router = Router();
const categoryController = new CategoryController();

/**
 * Get all Categories
 * @swagger
 * /categories:
 *   get:
 *     description: Returns categories
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: categories
 */
router.get(
  '/',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  categoryController.get
);

// Get one Category
router.get(
  '/:id',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  categoryController.getOneById
);

router.post(
  '/',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  categoryController.create
);

//Edit one category
router.patch(
  '/:id',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  categoryController.edit
);

//Delete Category
router.delete(
  '/:id',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  categoryController.delete
);

export default router;
