import { Router } from 'express';
import { checkJwt } from '../middlewares/checkJwt';
import { checkRole } from '../middlewares/checkRole';
import { ROLES } from '../utils/Roles.enum';
import ChallengeController from '../controllers/challenge.controller';

const router = Router();
const challengeController = new ChallengeController();

/**
 * Get all Challenges
 * @swagger
 * /challenges:
 *   get:
 *     description: Returns challenges
 *     tags:
 *      - Challenge
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: challenges
 */
router.get(
  '/',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  challengeController.get
);

// Get one challenge
router.get(
  '/:id',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  challengeController.getOneById
);

router.post(
  '/',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  challengeController.create
);

//Edit one user
router.patch(
  '/:id',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  challengeController.edit
);

//Delete Challenge
router.delete(
  '/:id',
  [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])],
  challengeController.delete
);

export default router;
