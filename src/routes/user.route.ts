import { Router } from 'express';
import UserController from '../controllers/user.controller';
import { checkJwt } from '../middlewares/checkJwt';
import { checkRole } from '../middlewares/checkRole';

const router = Router();
const userController = new UserController();

//Get all users
router.get('/', checkJwt, userController.get);

// Get one user
router.get('/:id', checkJwt, userController.getOneById);

//Create a new user
router.post('/', userController.create);

//Edit one user
router.patch('/:id', [checkJwt, checkRole(['ADMIN'])], userController.edit);

//Delete one user
router.delete('/:id', [checkJwt, checkRole(['ADMIN'])], userController.delete);

export default router;
