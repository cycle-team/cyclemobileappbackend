import { Router, Request, Response } from 'express';
import auth from './auth.route';
import user from './user.route';
import article from './article.route';
import challenge from './challenge.route';
import group from './group.route';
import category from './category.route';
import tag from './tag.route';

const routes = Router();

routes.use('/auth', auth);
routes.use('/users', user);
routes.use('/groups', group);
routes.use('/articles', article);
routes.use('/challenges', challenge);
routes.use('/categories', category);
routes.use('/tags', tag);

export default routes;
