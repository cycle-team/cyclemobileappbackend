import { Router } from 'express';
import GroupController from '../controllers/group.controller';
import { checkJwt } from '../middlewares/checkJwt';
import { checkRole } from '../middlewares/checkRole';
import { ROLES } from "../utils/Roles.enum";

const router = Router();
const groupController = new GroupController();

//Get groups
router.get('/', [checkJwt, checkRole(['ADMIN', 'USER'])], groupController.get);

// Get one group  // TODO ([a-f0-9]+) add regex verification in path
router.get('/:id', [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])], groupController.getOneById);

//Create a new group
router.post('/', [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])], groupController.create);

//Edit one group
router.patch('/:id', [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])], groupController.edit);

//Delete one group
router.delete('/:id', [checkJwt, checkRole([ROLES.ADMIN, ROLES.USER])], groupController.remove);

export default router; 