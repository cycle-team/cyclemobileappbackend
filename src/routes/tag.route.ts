import { Router } from 'express';
// Middlewares
import { checkJwt } from '../middlewares/checkJwt';
import { checkRole } from '../middlewares/checkRole';
// Personal Import
import { ROLES } from '../utils/Roles.enum';
import TagController from '../controllers/tag.controller';

const router = Router();
const tagController = new TagController();

// ? TODO: Doc ?
/**
 * Get all Tags
 * @swagger
 * /tags:
 *   get:
 *     description: Returns tags
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: tags
 */
router.get('/', [checkJwt], tagController.get);

// Get one tag
router.get('/:id', [checkJwt], tagController.getOneById);

router.post('/', [checkJwt], tagController.create);

//Edit one tag
router.patch('/:id', [checkJwt, checkRole([ROLES.ADMIN])], tagController.edit);

//Delete Tag
router.delete(
  '/:id',
  [checkJwt, checkRole([ROLES.ADMIN])],
  tagController.delete
);

export default router;
