import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
// Validators
import { Length, IsNotEmpty } from 'class-validator';
// Entity for relations
import { User } from './user.entity';
import { Challenge } from './challenge.entity';

//TODO: Faire la validation des datas

/**
 * The group entity
 */
@Entity()
export class Group {
  constructor(group?: Group) {
    if (group) {
      Object.assign(this, group);
    }
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  @Length(4, 200)
  @IsNotEmpty()
  name: string;

  @Column()
  @Length(4, 500)
  description: string;

  @Column()
  @Length(4, 1000)
  image: string;

  @Column()
  isActive: boolean;

  // Relations
  @ManyToMany((type) => User, (user) => user.groups)
  @JoinTable()
  users: User[];

  @ManyToMany((type) => Challenge, (challenge) => challenge.groups, {
    nullable: true,
  })
  @JoinTable()
  challenges: Challenge[];

  //TODO: Add Category Relation & Tag Relations

  /**
   * Default returned columns
   */
  static getColumns() {
    return ['id', 'name', 'description', 'image', 'isActive'];
  }
}
