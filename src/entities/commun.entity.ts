import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { MODERATIONSTATE } from '../utils/moderationState.enum';

//TODO: Faire la validation des datas

/**
 * The common entity
 */
export abstract class Commun {
  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @Column({ type: 'varchar', length: '2' })
  lang: string;

  @CreateDateColumn()
  createAt;

  @UpdateDateColumn()
  updateAt?;

  @Column({ type: 'varchar', length: '60' })
  title: string;

  @Column({ type: 'simple-json' })
  listImage: string;

  // pour mise en forme coté client. Contenu de quelque chosoe.
  @Column({ type: 'simple-json' })
  content: string;

  @Column({ type: 'enum', enum: MODERATIONSTATE })
  modState: MODERATIONSTATE;

  @Column({ type: 'bool' })
  isActive: boolean;

  @Column({ type: 'bool' })
  isDelete: boolean;
}
