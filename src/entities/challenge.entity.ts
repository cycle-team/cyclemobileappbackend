import {
  Entity,
  Column,
  JoinTable,
  ManyToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
// Validators
import { Length, IsNotEmpty } from 'class-validator';
// Entity for relations
import { User } from './user.entity';
import { Commun } from './commun.entity';
import { Tag } from './tag.entity';
import { Group } from './group.entity';
import { Category } from './category.entity';
// Enums
import { difficultyChallenge } from '../utils/challenge/difficultyChallenge.enum';
import { challengeFrequency } from '../utils/challenge/frequencyChallenge.enum';
import { MODERATIONSTATE } from '../utils/moderationState.enum';

//TODO: Faire la validation des datas

/**
 * The challenge entity
 */
@Entity()
export class Challenge extends Commun {
  constructor(challenge?: Challenge) {
    super();
    //
    if (challenge) {
      challenge.modState = MODERATIONSTATE.DRAFT;
      Object.assign(this, challenge);
    }
  }

  @Column({ type: 'text', nullable: false })
  intro: string;

  @Column({ type: 'text' })
  desc: string;

  @Column({
    type: 'enum',
    enum: difficultyChallenge,
    default: difficultyChallenge.MIDDLE,
  })
  difficulty: difficultyChallenge;

  @Column({
    type: 'enum',
    enum: challengeFrequency,
    default: challengeFrequency.DAY,
  })
  frequency: challengeFrequency;

  // @Column({ type: 'blob' })
  @Column({ type: 'text' })
  coverImg: string;

  @Column({ type: 'simple-json' })
  sources: string[];

  @ManyToOne((type) => User, (user) => user.challenges)
  @JoinColumn()
  authorChallenge: User;

  @ManyToMany((type) => Group, (group) => group.challenges, { nullable: true })
  groups: Group[];

  @ManyToMany((type) => Tag, (tag) => tag.challenges, {
    nullable: true,
    cascade: true,
  })
  @JoinTable()
  tags: Tag[];

  @ManyToOne((type) => Category, (category) => category.challenges)
  category: Category;

  /**
   * Default returned columns
   */
  static getColumns() {
    return [
      'id',
      'lang',
      'createAt',
      'updateAt',
      'title',
      'coverImg',
      'listImage',
      'frequency',
      'difficulty',
      'intro',
      'content',
      'modState',
      'isActive',
      'isDelete',
    ];
  }
}
