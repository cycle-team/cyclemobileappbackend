import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { CategoryModel } from '../models/category.model';
// Validators
import { Length, IsNotEmpty } from 'class-validator';
// Entity for relations
import { Challenge } from './challenge.entity';
import { Article } from './article.entity';

//TODO: Faire la validation des datas

/**
 * The category entity
 */
@Entity()
export class Category {
  constructor(category?: CategoryModel) {
    if (category) {
      Object.assign(this, category);
    }
  }

  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @Column({ type: 'varchar', length: '2' })
  lang: string;

  @Column({ type: 'varchar', length: '60' })
  name: string;

  // Relations
  @OneToMany((type) => Article, (article) => article.category)
  articles: Article[];

  @OneToMany((type) => Challenge, (challenge) => challenge.category)
  challenges: Challenge[];

  /**
   * Default returned columns
   */
  static getColumns() {
    return ['id', 'lang', 'name'];
  }
}
