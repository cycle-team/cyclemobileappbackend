import { Article } from './article.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  CreateDateColumn,
} from 'typeorm';
// Validators
import { Length, IsNotEmpty } from 'class-validator';
// Entity for relations
import { Challenge } from './challenge.entity';
import { TagModel } from '../models/tag.model';

//TODO: Faire la validation des datas

/**
 * The tag entity
 */
@Entity()
export class Tag {
  constructor(tag?: Tag | TagModel) {
    if (tag) {
      Object.assign(this, tag);
    }
  }

  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @Column({ type: 'varchar', length: '2' })
  public lang: string;

  @Column({ type: 'varchar', length: '60' })
  name: string;

  @Column()
  count: number;

  @Column()
  popularity: number;

  @CreateDateColumn()
  public createAt?;

  @ManyToMany((type) => Challenge, (challenge) => challenge.tags)
  challenges?: Challenge[];

  /*   // ? TODO: Question sur le type des articles dans tags : Stockage uniquement de l'id ?
  @ManyToMany((type) => Article, (article) => article.tags)
  tags?: Article[]; */

  /**
   * Default returned columns
   */
  static getColumns() {
    return ['id', 'lang', 'name', 'count', 'popularity', 'createAt'];
  }
}
