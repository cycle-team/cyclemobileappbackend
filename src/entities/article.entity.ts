import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  ManyToMany,
  JoinTable,
  Unique,
} from 'typeorm';
// Validators
import { Length, IsNotEmpty } from 'class-validator';
// Entity for relations
import { User } from './user.entity';
import { Tag } from './tag.entity';
import { Category } from './category.entity';
import { ArticleModel } from '../models/article.model';

//TODO: Faire la validation des datas

/**
 * The user entity
 */
@Entity()
@Unique(['title'])
export class Article {
  constructor(article?: Article | ArticleModel) {
    if (article) {
      Object.assign(this, article);
    }
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  @Length(4, 200)
  @IsNotEmpty()
  title: string;

  @Column()
  @Length(4, 500)
  description: string;

  @Column()
  @Length(4, 1000)
  content: string;

  @Column()
  @Length(4, 500)
  image: string;

  @Column({
    nullable: true,
  })
  like: number;

  @Column({
    nullable: true,
  })
  views: number;

  @Column()
  published: boolean;

  // Relations
  @ManyToOne((type) => User, (user) => user.articles)
  user: User;

  @ManyToOne((type) => Category, (category) => category.articles)
  category: Category;

  @ManyToMany((type) => Tag)
  @JoinTable()
  tags: Tag[];

  /**
   * Default returned columns
   */
  static getColumns() {
    return [
      'id',
      'title',
      'description',
      'content',
      'image',
      'like',
      'views',
      'published',
    ];
  }
}
