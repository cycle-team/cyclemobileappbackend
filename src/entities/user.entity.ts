import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  OneToMany,
  ManyToMany,
} from 'typeorm';
// Utils
import * as bcrypt from 'bcryptjs';
// Validators
import { Length, IsNotEmpty } from 'class-validator';
// Entity for relations
import { Article } from './article.entity';
import { Group } from './group.entity';
import { Challenge } from './challenge.entity';
import { UserModel } from '../models/user.model';

//TODO: Faire la validation des datas

/**
 * The user entity
 */
@Entity()
@Unique(['username'])
export class User {
  constructor(user?: UserModel | User) {
    if (user) {
      Object.assign(this, user);
    }
  }

  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @Column()
  //@Length(4, 20)
  gender: string;

  @Column()
  //@Length(4, 20)
  birthday?: string;

  @Column()
  //@Length(4, 20)
  age?: Number;

  @Column()
  //@Length(4, 20)
  username: string;

  @Column()
  //@Length(4, 100)
  password?: string;

  @Column()
  @IsNotEmpty()
  role: string;

  // Realations
  @ManyToMany((type) => Group, (group) => group.users)
  groups: Group[];

  @OneToMany((type) => Article, (article) => article.user)
  articles?: Article[];

  @OneToMany((type) => Challenge, (challenge) => challenge.authorChallenge, {
    cascade: false,
  })
  challenges?: Challenge[];

  // Methodes
  /**
   * Hash the user password
   */
  hashPassword() {
    this.password = bcrypt.hashSync(this.password, 8);
  }

  /**
   * Check if the user password is valid
   *
   * @param unencryptedPassword The user password
   */
  checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
    return bcrypt.compareSync(unencryptedPassword, this.password);
  }

  /**
   * Default returned columns
   */
  static getColumns() {
    return ['id', 'gender', 'birthday', 'age', 'username', 'role'];
  }
}
