import { MigrationInterface, QueryRunner, getConnection } from 'typeorm';
import { Category } from '../entities/category.entity';
import { categoriesMock } from '../mocks/category.mock';
import { CategoryRepository } from '../repositories/category.repository';
import { CategoryModel } from '../models/category.model';

export class CreateCategory1586977427417 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    CategoryRepository.getInstance(getConnection());
    let categories = [];
    categoriesMock.forEach(async (cat: CategoryModel) => {
      let category = new Category(cat);
      categories.push(category);
    });
    await CategoryRepository.getInstance()
      .getCategoryRepository()
      .save(categories);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
