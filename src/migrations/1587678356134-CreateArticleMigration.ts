import { MigrationInterface, QueryRunner, getConnection } from 'typeorm';
import { ArticleRepository } from '../repositories/article.repository';
import { articlesMock } from '../mocks/article.mock';
import { ArticleModel } from '../models/article.model';
import { Article } from '../entities/article.entity';

export class ArticleController1587678356134 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    ArticleRepository.getInstance(getConnection());
    let articles = [];
    articlesMock.forEach(async (art: ArticleModel) => {
      let article = new Article(art);
      articles.push(article);
    });
    await ArticleRepository.getInstance().getArticleRepository().save(articles);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
