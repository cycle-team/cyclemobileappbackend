import { MigrationInterface, QueryRunner, getConnection } from 'typeorm';
// Personnal Import
import { Tag } from './../entities/tag.entity';
import { TagModel } from './../models/tag.model';
import { TagRepository } from './../repositories/tag.repository';
import { tagsMock } from '../mocks/tag.mock';

/**
 * ? Create somes tags
 *
 */
export class CreateTags1587822945711 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    TagRepository.getInstance(getConnection());
    let tags = [];
    tagsMock.forEach(async (tag: TagModel) => {
      let tagCreate = new Tag(tag);
      tags.push(tagCreate);
    });
    await TagRepository.getInstance().getTagRepository().save(tags);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
