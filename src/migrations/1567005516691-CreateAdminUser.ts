import { MigrationInterface, QueryRunner, getConnection } from "typeorm";
import { UserRepository } from "../repositories/user.repository";
import { User } from "../entities/user.entity";
import { userMock } from "../mocks/user.mock";

export class CreateAdminUser1567005516691 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    UserRepository.getInstance(getConnection());
    let user = new User(userMock);
    user.hashPassword();
    await UserRepository.getInstance().getUserRepository().save(user);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
