import { Request, Response } from 'express';
import { getRepository, Repository } from 'typeorm';
import { validate } from 'class-validator';

import { User } from '../entities/user.entity';
import { UserRepository } from '../repositories/user.repository';
import { UserModel } from '../models/user.model';

class UserService {
  /**
   * Find users from database
   *
   * @param columns Columns to select from database
   */
  find = async (columns: any[]) => {
    let users;

    // get users from database
    try {
      users = await UserRepository.getInstance()
        .getUserRepository()
        .find({
          select: columns,
          relations: ['groups'],
        });
    } catch (error) {
      return error;
    }

    // Return users
    return users;
  };

  /**
   * Find user from database
   *
   * @param columns Columns to select from database
   */
  findOne = async (id: string, columns: any[]) => {
    // get the user from database
    let user;
    try {
      user = await UserRepository.getInstance()
        .getUserRepository()
        .findOneOrFail(id, {
          select: columns,
        });
    } catch (error) {
      return error;
    }

    return user;
  };

  /**
   * Insert user to database
   *
   * @param user The user to insert to database
   */
  insertOne = async (user: UserModel | User): Promise<UserModel | User> => {
    //Get parameters from the body
    let userToCreate: UserModel | User = new User(user);

    //Validade if the parameters are ok
    const errors = await validate(user);

    if (errors.length > 0) {
      return;
    }

    //Hash the password, to securely store on DB
    userToCreate.hashPassword();

    //Try to save. If fails, the username is already in use
    try {
      return await UserRepository.getInstance()
        .getUserRepository()
        .save(userToCreate);
    } catch (error) {
      return error;
    }
  };

  /**
   * Update user from database
   *
   * @param user The user to update to database
   */
  findOneAndUpdate = async (user: User) => {
    let userToEdit;
    //Try to safe, if fails, that means username already in use
    try {
      userToEdit = await UserRepository.getInstance()
        .getUserRepository()
        .save(user);
    } catch (error) {
      return error;
    }
    //After all send a 204 (no content, but accepted) response
    return userToEdit;
  };

  /**
   * Delete user from database
   *
   * @param id The user id to delete
   */
  findOneAndDelete = async (id: string) => {
    let data;
    // Get data from database
    try {
      const user = await UserRepository.getInstance()
        .getUserRepository()
        .findOneOrFail(id);
      data = await UserRepository.getInstance()
        .getUserRepository()
        .delete(user);
    } catch (error) {
      return error;
    }
    // Return data
    return data;
  };
}

export default UserService;
