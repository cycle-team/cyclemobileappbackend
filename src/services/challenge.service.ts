import { DeleteResult } from 'typeorm';
import { validate } from 'class-validator';
// Repositories
import { ChallengeRepository } from '../repositories/challenge.repository';
// Servicies
import TagService from './tag.service';
// Entities
import { Challenge } from '../entities/challenge.entity';
import { Tag } from '../entities/tag.entity';

/**
 * Challenge service
 */
class ChallengeService {
  // init tag service
  tagService: TagService;

  /**
   * The challenge constructor
   *
   * Inject Tag service
   */
  constructor() {
    this.tagService = new TagService();
  }

  //TODO: Find & findOne => Uniquemnt les challenge isActive et !isDelete

  /**
   * Find all challenges
   *
   * @param {string} id The challenge id
   *
   * @returns {Promise<Challenge[]>} Return an promise who resolve the founded challenges
   */
  find = async (columns: any[]): Promise<Challenge[]> => {
    return new Promise(async (resolve, reject) => {
      try {
        const challenges = await ChallengeRepository.getInstance()
          .getChallengeRepository()
          .find({
            select: columns,
            relations: ['groups', 'category', 'tags'],
          });
        resolve(challenges);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find one challenge by id
   *
   * @param {string} id The challenge id
   *
   * @returns {Promise<Challenge>} Return an promise who resolve the founded challenge
   */
  findOne = async (id: string, columns: any[]): Promise<Challenge> => {
    return new Promise(async (resolve, reject) => {
      try {
        const challenge = await ChallengeRepository.getInstance()
          .getChallengeRepository()
          .findOneOrFail(id, {
            select: columns,
          });
        resolve(challenge);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Create an challenge
   *
   * @param {Challenge} challenge The challenge to create
   *
   * @returns {Promise<Challenge>} Return an promise who resolve the created challenge
   */
  insertOne = async (challenge: Challenge): Promise<Challenge> => {
    return new Promise(async (resolve, reject) => {
      let challengeToCreate = new Challenge(challenge);

      // init all tags array
      let allTags = [];

      // use Promise.all() to attemp tag creation if not already exist
      await Promise.all(
        challenge.tags.map(async (tag) => {
          if (!tag.id) {
            allTags.push(await this.tagService.insertOne(tag));
          } else {
            allTags.push(
              await this.tagService.findOne(tag.id, Tag.getColumns())
            );
          }
        })
      );
      // assign all tags to model
      challengeToCreate.tags = allTags;

      // try to save challenge
      try {
        const challenge = await ChallengeRepository.getInstance()
          .getChallengeRepository()
          .save(challengeToCreate);

        resolve(challenge);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find one and update challenge by id
   *
   * @param {Challenge} challenge The challenge object properties to update
   *
   * @returns {Promise<Challenge>} Return an promise who resolve the updated challenge
   */
  findOneAndUpdate = async (challenge: Challenge): Promise<Challenge> => {
    return new Promise(async (resolve, reject) => {
      try {
        let challengeUpdated;
        await this.findOne(challenge.id, Challenge.getColumns()).then(
          async () => {
            await ChallengeRepository.getInstance()
              .getChallengeRepository()
              .save(challenge)
              .then(async () => {
                challengeUpdated = await this.findOne(
                  challenge.id,
                  Challenge.getColumns()
                );
                resolve(challengeUpdated);
              })
              .catch((error) => {
                reject(error);
              });
          }
        );
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find and delete an challenge by id
   *
   * @param id The challenge id
   *
   * @returns {Promise} Return empty prmise
   */
  findOneAndDelete = async (id: string): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      await ChallengeRepository.getInstance()
        .getChallengeRepository()
        .delete(id)
        .then((result: DeleteResult) => {
          if (result.affected !== 0) {
            resolve();
          } else {
            reject({
              name: 'NotExist',
              message: `Challenge with id ${id} not found.`,
            });
          }
        });
    });
  };
}

// Export ChallengeService
export default ChallengeService;
