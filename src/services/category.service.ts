// Personnal Import
import { DeleteResult } from 'typeorm';
import { Category } from '../entities/category.entity';
import { CategoryRepository } from '../repositories/category.repository';

/**
 *
 * * Category Service
 */
class CategoryService {
  /**
   * Find all categories
   *
   * @param {array} columns Columns to select from database
   *
   * @return {Promise<Category[]>} Return an promise who resolve the founded categories
   */
  find = async (columns: any[]): Promise<Category[]> => {
    return new Promise(async (resolve, reject) => {
      try {
        const categories = await CategoryRepository.getInstance()
          .getCategoryRepository()
          .find({
            select: columns,
            relations: ['challenges', 'articles'],
          });
        resolve(categories);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find category by id
   *
   * @param {string} id The category id
   *
   * @returns {Promise<Category>} Return an promise who resolve the founded category
   */
  findOne = async (id: string, columns: any[]): Promise<Category> => {
    return new Promise(async (resolve, reject) => {
      try {
        const category = await CategoryRepository.getInstance()
          .getCategoryRepository()
          .findOneOrFail(id, {
            select: columns,
          });
        resolve(category);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Create an challenge
   *
   * @param {Category} category The category to create
   *
   * @returns {Promise<Category>} Return an promise who resolve the created category
   *
   */
  insertOne = async (category: Category): Promise<Category> => {
    return new Promise(async (resolve, reject) => {
      let categoryToCreate = new Category(category);
      try {
        const category = await CategoryRepository.getInstance()
          .getCategoryRepository()
          .save(categoryToCreate);
        resolve(category);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find one and update category by id
   *
   * @param {category} category The Category object propertiies to update
   *
   * @returns {Promise<Category>} Return an promise who resolve the updated category
   */
  findOneAndUpdate = async (category: Category): Promise<Category> => {
    return new Promise(async (resolve, reject) => {
      try {
        let categoryUpdated;
        await this.findOne(category.id, Category.getColumns()).then(
          async () => {
            await CategoryRepository.getInstance()
              .getCategoryRepository()
              .save(category)
              .then(async () => {
                categoryUpdated = await this.findOne(
                  category.id,
                  Category.getColumns()
                );
                resolve(categoryUpdated);
              })
              .catch((e) => {
                reject(e);
              });
          }
        );
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find & Delete an cateogy by ID
   *
   * @param id The category id to delete
   *
   * @returns {Promise} Return empty promise
   */
  findOneAndDelete = async (id: string): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      await CategoryRepository.getInstance()
        .getCategoryRepository()
        .delete(id)
        .then((result: DeleteResult) => {
          if (result.affected !== 0) {
            resolve();
          } else {
            reject({
              name: 'NotExist',
              message: `Category with id ${id} not found.`,
            });
          }
        });
    });
  };
}

export default CategoryService;
