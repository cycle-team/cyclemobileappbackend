import { validate } from 'class-validator';

import { ArticleRepository } from '../repositories/article.repository';
import { Article } from '../entities/article.entity';
import TagService from './tag.service';
import { Tag } from '../entities/tag.entity';
import { DeleteResult } from 'typeorm';

/**
 * Article service
 */
class ArticleService {
  // init tag service
  tagService: TagService;

  /**
   * The article constructor
   *
   * Inject Tag service
   */
  constructor() {
    this.tagService = new TagService();
  }

  /**
   * Find all articles
   *
   * @param {Array<any>} columns The Article entity columns
   *
   * @returns {Promise<Article[]>} Return a promise who resolve the founded articles
   */
  find = async (columns: any[]): Promise<Article[]> => {
    return new Promise(async (resolve, reject) => {
      try {
        const articles = await ArticleRepository.getInstance()
          .getArticleRepository()
          .find({
            select: columns,
            relations: ['category', 'user', 'tags'],
          });
        resolve(articles);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find one article by id
   *
   * @param {string} id The article id
   * @param {Array<any>} columns The Article entity column
   *
   * @returns {Promise<Challenge>} Return an promise who resolve the founded article
   */
  findOne = async (id: string, columns: any[]): Promise<Article> => {
    return new Promise(async (resolve, reject) => {
      try {
        const article = await ArticleRepository.getInstance()
          .getArticleRepository()
          .findOneOrFail(id, {
            select: columns,
            relations: ['category', 'user', 'tags'],
          });
        resolve(article);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Create an article
   *
   * @param {Challenge} challenge The article to create
   *
   * @returns {Promise<Challenge>} Return an promise who resolve the created article
   */
  insertOne = async (article: Article): Promise<Article> => {
    return new Promise(async (resolve, reject) => {
      let articleToCreate = new Article(article);

      // init all tags array
      let allTags = [];

      await Promise.all(
        article.tags.map(async (tag) => {
          if (!tag.id) {
            allTags.push(await this.tagService.insertOne(tag));
          } else {
            allTags.push(
              await this.tagService.findOne(tag.id, Tag.getColumns())
            );
          }
        })
      );
      // assign all tags to model
      articleToCreate.tags = allTags;

      // Try to save article
      try {
        const article = await ArticleRepository.getInstance()
          .getArticleRepository()
          .save(articleToCreate);
        resolve(article);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find one and update article
   *
   * @param {Article} article The article object properties to update
   *
   * @returns {Promise<Challenge>} Return an promise who resolve the updated article
   */
  findOneAndUpdate = async (article: Article): Promise<Article> => {
    return new Promise(async (resolve, reject) => {
      try {
        let articleUpdated;
        await this.findOne(article.id, Article.getColumns()).then(async () => {
          await ArticleRepository.getInstance()
            .getArticleRepository()
            .save(article)
            .then(async () => {
              articleUpdated = await this.findOne(
                article.id,
                Article.getColumns()
              );
              resolve(articleUpdated);
            })
            .catch((error) => {
              reject(error);
            });
        });
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * Find and delete an article by id
   *
   * @param id The challenge id
   *
   * @returns {Promise} Return empty promise
   */
  findOneAndDelete = async (id: string) => {
    return new Promise(async (resolve, reject) => {
      await ArticleRepository.getInstance()
        .getArticleRepository()
        .delete(id)
        .then((result: DeleteResult) => {
          if (result.affected !== 0) {
            resolve();
          } else {
            reject({
              name: 'NotExist',
              message: `Article with id ${id} not found.`,
            });
          }
        });
    });
  };
}

export default ArticleService;
