// Personnal Import
import { Tag } from '../entities/tag.entity';
import { TagRepository } from '../repositories/tag.repository';
import { TagModel } from '../models/tag.model';
import { DeleteResult } from 'typeorm';

/**
 *
 * * Tag Service
 */
class TagService {
  /**
   * * Find categories from database
   *
   * @param {array} columns Columns to select from database
   *
   * @return {Promise<Tag[]>} Return an promise who resolve the founded tags
   */
  find = async (columns: any[]): Promise<Tag[]> => {
    return new Promise(async (resolve, reject) => {
      try {
        const tags = await TagRepository.getInstance()
          .getTagRepository()
          .find({
            select: columns,
            // ? TODO: relations: ['challenges', 'articles'],
            relations: ['challenges'],
          });
        resolve(tags);
      } catch (e) {
        reject(e);
      }
    });
  };

  /**
   * * Find Tag by id
   *
   * @param {string} id The tag id
   *
   * @returns {Promise<Tag>} Return an promise who resolve the founded tag
   *
   */
  findOne = async (id: string, columns: any[]): Promise<Tag> => {
    return new Promise(async (resolve, reject) => {
      // get the user from database
      try {
        const tag = await TagRepository.getInstance()
          .getTagRepository()
          .findOneOrFail(id, {
            select: columns,
          });
        resolve(tag);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * * Create a Tag
   *
   * @param {Tag} tag The Tag to create
   *
   * @returns {Promise<Tag>} Return an promise who resolve the created tag
   *
   */
  insertOne = async (tag: Tag | TagModel): Promise<Tag> => {
    return new Promise(async (resolve, reject) => {
      let tagToCreate = new Tag(tag);
      try {
        const tag = await TagRepository.getInstance()
          .getTagRepository()
          .save(tagToCreate);
        resolve(tag);
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * * Find one and update tag by id
   *
   * @param {tag} tag The Tag object propertiies to update
   *
   * @returns {Promise<Tag>} Return an promise who resolve the updated tag
   */
  findOneAndUpdate = async (tag: Tag | TagModel): Promise<Tag> => {
    return new Promise(async (resolve, reject) => {
      try {
        let tagUpdated;
        await this.findOne(tag.id, Tag.getColumns()).then(async () => {
          await TagRepository.getInstance()
            .getTagRepository()
            .save(tag)
            .then(async () => {
              tagUpdated = await this.findOne(tag.id, Tag.getColumns());
              resolve(tagUpdated);
            })
            .catch((e) => {
              reject(e);
            });
        });
      } catch (error) {
        reject(error);
      }
    });
  };

  /**
   * * Find & Delete a tag by ID
   *
   * @param id The tag id to delete
   *
   * @returns {Promise} Return empty promise
   */
  findOneAndDelete = async (id: string): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      await TagRepository.getInstance()
        .getTagRepository()
        .delete(id)
        .then((result: DeleteResult) => {
          if (result.affected !== 0) {
            resolve();
          } else {
            reject({
              name: 'NotExist',
              message: `Tag with id ${id} not found.`,
            });
          }
        });
    });
  };
}

export default TagService;
