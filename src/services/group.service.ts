import { validate } from "class-validator";

import { GroupRepository } from "../repositories/group.repository";
import { Group } from "../entities/group.entity";

class GroupService {
  /**
   * Find users from database
   *
   * @param columns Columns to select from database
   */
  find = async (columns: any[]) => {
    let groups;
    // get users from database
    try {
      groups = await GroupRepository.getInstance()
        .getGroupRepository()
        .find({
          select: columns,
          relations: ["users"],
        });
    } catch (error) {
      return error;
    }

    // Return groups
    return groups;
  };

  findOne = async (id: string, columns: any[]) => {
    let group;
    try {
      group = await GroupRepository.getInstance()
        .getGroupRepository()
        .findOne(id, {
          select: columns,
        });
    } catch (error) {
      return error;
    }

    // Return group
    return group;
  };

  insertOne = async (group: Group) => {
    //Get parameters from the body
    let groupToCreate = new Group(group);

    //Validade if the parameters are ok
    const errors = await validate(group);

    if (errors.length > 0) {
      return;
    }

    //Try to save. If fails, the username is already in use
    try {
      return await GroupRepository.getInstance()
        .getGroupRepository()
        .save(groupToCreate);
    } catch (error) {
      return error;
    }
  };

  findOneAndUpdate = async (group: Group) => {
    let groupToEdit;
    //Try to safe, if fails, that means title already in use
    try {
      groupToEdit = await GroupRepository.getInstance()
        .getGroupRepository()
        .save(group);
    } catch (error) {
      return error;
    }
    //After all send a 204 (no content, but accepted) response
    return groupToEdit;
  };

  findOneAndDelete = async (id: string) => {
    let data;
    // Get data from database
    try {
      const group = await GroupRepository.getInstance()
        .getGroupRepository()
        .findOneOrFail(id);
      data = await GroupRepository.getInstance()
        .getGroupRepository()
        .delete(group);
    } catch (error) {
      return error;
    }
    // Return data
    return data;
  };
}

export default GroupService;
