import { CategoryModel } from '../models/category.model';

/**
 * Categories Array Mock
 *
 * ? Mock use for Migration : 1586977427417-CreateCategory
 */
export const categoriesMock: CategoryModel[] = [
  {
    id: '5b7733be-3850-47e7-8fa5-3b4e098c574',
    lang: 'fr',
    name: 'Cultivation',
  },
  {
    id: '5b7733be-3850-47e7-8fa5-3b4e788c574',
    lang: 'fr',
    name: 'Pollution',
  },
  {
    id: '5b7733be-3450-47e7-8fa5-3b4e098c573',
    lang: 'fr',
    name: 'Gestion des déchets',
  },
  {
    id: '5b2433be-3850-47e7-8fa5-3b4e098c574',
    lang: 'fr',
    name: 'Consommation durable',
  },
  {
    id: '5b7733be-3850-47e7-8ha5-3b4e098c574',
    lang: 'fr',
    name: 'Energie',
  },
];

/**
 * Category Object
 *
 * ? Mocks use for Jest Testing
 *
 */
export const categoryToCreateMock = {
  id: 'CategoryCreatedByJest',
  lang: 'fr',
  name: 'Category created by Jest',
};

export const categoryMock: CategoryModel = {
  id: '5b7745be-3850-47e7-8fa5-3b4e78bc574',
  lang: 'fr',
  name: 'Consomation durable',
};

export const categoryEdit = {
  id: 'CategoryCreatedByJest',
  lang: 'fr',
  name: 'Category created by Jest & Modify',
};
