import { UserModel } from '../models/user.model';

export const userMock: any = {
  id: '5b7725be-3850-47e7-8fa5-3b4e43bc574f',
  username: 'admin',
  password: 'admin',
  groups: [],
  role: 'ADMIN',
  gender: 'M',
  birthday: '1996-09-25',
  age: 22,
  articles: [],
  challenges: [],
};

export const usersMock: any[] = [
  {
    id: '5b7725be-3850-47e7-8fa5-3b4e43bc574f',
    username: 'admin',
    password: 'admin',
    groups: [],
    role: 'ADMIN',
    gender: 'M',
    birthday: '1996-09-25',
    age: 22,
    articles: [],
    challenges: [],
  },
  {
    id: '6cr725be-3850-47e7-8fa5-3b4e43bc574f',
    username: 'user',
    password: 'user',
    groups: [],
    role: 'USER',
    gender: 'M',
    birthday: '1996-09-25',
    age: 22,
    articles: [],
    challenges: [],
  },
];

export const userToCreateMock = {
  username: 'userCreated',
  password: 'password',
  role: 'USER',
  gender: 'homme',
  birthday: '1996-09-25',
  age: 23,
  groups: [],
  articles: [],
  challenges: [],
};
