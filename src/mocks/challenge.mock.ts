import { difficultyChallenge } from '../utils/challenge/difficultyChallenge.enum';
import { challengeFrequency } from '../utils/challenge/frequencyChallenge.enum';
import { MODERATIONSTATE } from '../utils/moderationState.enum';
import { ChallengeModel } from '../models/challenge.model';

export const challengeMock: ChallengeModel = {
  lang: 'fr',
  title: 'Challenge Create by API',
  listImage: [
    'https://i.picsum.photos/id/1024/200/500.jpg',
    'https://i.picsum.photos/id/1024/200/500.jpg',
  ],
  content: [
    { style: 'text', content: 'Texte du challenge' },
    { style: 'image', content: 'https://i.picsum.photos/id/1024/200/500.jpg' },
    { style: 'text', content: 'Nouveau texte du Challenge Create by API' },
  ],
  intro: "Texte d'introduction du Challenge creer par API.",
  desc: 'Short Description du Challenge creer par API',
  difficulty: difficultyChallenge.HIGH,
  frequency: challengeFrequency.DAY,
  coverImg: 'https://i.picsum.photos/id/1024/200/500.jpg',
  sources: ['www.google.com', 'https://www.aston-ecole.com/'],
  tags: [
    {
      lang: 'fr',
      name: 'TAGCreated',
      count: 10,
      popularity: 10,
      challenges: [],
    },
    {
      id: '1',
    },
  ],
  category: {
    lang: 'fr',
    name: 'Eau',
  },
  modState: MODERATIONSTATE.DRAFT,
  authorChallenge: {
    id: '10638ce5-52ab-4bc3-840a-82214d4d2ffa',
    username: 'Meums',
    gender: 'H',
    role: 'USER',
    groups: [],
  },
  createAt: '2020-02-01',
  updateAt: '2020-02-05',
  isActive: true,
  isDelete: false,
};

export const challengesMock: ChallengeModel[] = [challengeMock, challengeMock];
