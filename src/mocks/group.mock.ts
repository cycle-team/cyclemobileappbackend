import { GroupModel } from "../models/group.model";

export const groupMock: GroupModel = {
  name: "Cycle Team",
  description: "Le groupe des admins",
  image: "https://i.picsum.photos/id/1024/200/500.jpg",
  isActive: true,
  users: [],
};

export const groupsMock: GroupModel[] = [groupMock, groupMock];
