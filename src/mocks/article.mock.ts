import { ArticleModel } from '../models/article.model';

export const enum ARTICLE_MOCK_ID {
  DEFAULT = 'articleDefault',
  CREATE = 'articleTestCreate',
  EDIT = 'articleTestEdit',
  DELETE = 'articleTestRemove',
  NOT_EXIST = 'notExist',
}

export const articlesMock: any[] = [
  {
    id: ARTICLE_MOCK_ID.DEFAULT,
    title: 'Article par défaut',
    description: 'Article par défaut',
    content:
      'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',
    category: { id: '5b7733be-3850-47e7-8fa5-3b4e098c574' },
    image:
      'https://github.com/typeorm/typeorm/raw/master/resources/logo_big.png',
    user: {
      id: '5b7725be-3850-47e7-8fa5-3b4e43bc574f',
    },
    published: true,
    like: 9,
    views: 10,
    tags: [],
  },
  {
    id: ARTICLE_MOCK_ID.EDIT,
    title: 'Article à éditer',
    description: 'Article à éditer',
    content:
      'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',
    category: { id: '5b7733be-3850-47e7-8fa5-3b4e098c574' },
    image:
      'https://github.com/typeorm/typeorm/raw/master/resources/logo_big.png',
    user: {
      id: '5b7725be-3850-47e7-8fa5-3b4e43bc574f',
    },
    published: true,
    like: 9,
    views: 10,
    tags: [],
  },
  {
    id: ARTICLE_MOCK_ID.DELETE,
    title: 'Article à effacer',
    description: 'Article à effacer',
    content:
      'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',
    category: { id: '5b7733be-3850-47e7-8fa5-3b4e098c574' },
    image:
      'https://github.com/typeorm/typeorm/raw/master/resources/logo_big.png',
    user: {
      id: '5b7725be-3850-47e7-8fa5-3b4e43bc574f',
    },
    published: true,
    like: 9,
    views: 10,
    tags: [],
  },
];

export const articleToCreateMock = {
  id: ARTICLE_MOCK_ID.CREATE,
  title: 'Article à créer',
  description: 'Article à créer',
  content:
    'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.',
  category: { id: '5b7733be-3850-47e7-8fa5-3b4e098c574' },
  image: 'https://github.com/typeorm/typeorm/raw/master/resources/logo_big.png',
  user: {
    id: '5b7725be-3850-47e7-8fa5-3b4e43bc574f',
  },
  published: true,
  like: 9,
  views: 10,
  tags: [],
};

export const articleMock: ArticleModel = articlesMock[0];

export const articleToEditMock = {
  id: ARTICLE_MOCK_ID.EDIT,
  title: 'Article édité',
  category: { id: '5b7733be-3450-47e7-8fa5-3b4e098c573' },
};
