import { TagModel } from './../models/tag.model';

export const tagsMock: TagModel[] = [
  {
    lang: 'fr',
    name: 'KiffTesDechets',
    count: 10,
    popularity: 30,
    challenges: [],
    id: '30cc0ca2-e3b4-4b3f-8e41-b28if34df8bf',
  },
  {
    lang: 'fr',
    name: 'ConsigneTonVerre',
    count: 15,
    popularity: 25,
    challenges: [],
    id: '30cc0ca2-e3b4-4b3f-8e41-b2mp034df8bf',
  },
  {
    lang: 'fr',
    name: 'DoucheDorée',
    count: 2,
    popularity: 0,
    challenges: [],
    id: '30cc0ca2-e3b4-4b3f-8e41-b2pyn34df8bf',
  },
  {
    lang: 'en',
    name: 'ZeroWaste',
    count: 102,
    popularity: 70,
    challenges: [],
    id: '30cc0ca2-e3b4-4b3f-8e41-b2pmf4df8bf',
  },
  {
    lang: 'fr',
    name: 'SaveOurOcean',
    count: 170,
    popularity: 80,
    challenges: [],
    id: '30cc0ca2-e3b4-4b3f-8e41-b2pob54df8bf',
  },
];

//? TODO: Add challenges before ok by migrations to the mock ?
export const tagToCreateMock = {
  id: 'TagCreateByJestTest',
  lang: 'fr',
  name: 'TagCreatedByJest',
  count: 120,
  popularity: 45,
  challenge: [],
};
