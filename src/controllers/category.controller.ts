import { Request, Response } from 'express';
// Services
import CategoryService from '../services/category.service';
// Entities
import { Category } from '../entities/category.entity';

/**
 * * The category controller
 */
class CategoryController {
  categoryService: CategoryService;

  /**
   * * The category constructor
   *
   * * Inject Category service
   */
  constructor() {
    this.categoryService = new CategoryService();
  }

  /**
   * * Get all categories
   *
   * * Call CategoryService find to get all categories
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status:'OK', data:Category[]} | 400 on error {status: 'ERROR}, error : {name:string, message:string}}
   *
   */
  get = (req: Request, res: Response) => {
    let response: Response;
    this.categoryService
      .find(Category.getColumns())
      .then((categories: Category[]) => {
        response = res
          .status(200)
          .send({ status: 'OK', data: categories, count: categories.length });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });
    return response;
  };

  /**
   * * Get a category
   *
   * * Call CategoryService find one to get a category
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} return: 200 on success {status:'OK', data:category} | 400 on error {status:'ERROR', error: {name:string, message:string }}
   */
  getOneById = async (req: Request, res: Response) => {
    let response: Response;
    const id: string = req.params.id;
    //
    this.categoryService
      .findOne(id, Category.getColumns())
      .then((category: Category) => {
        response = res.status(200).send({ status: 'OK', data: category });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });
    return response;
  };

  /**
   * * Create an category
   *
   * * Call categorySerice insertOne to create a category
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Category} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  create = async (req: Request, res: Response) => {
    let response: Response;
    let category: Category = req.body;

    this.categoryService
      .insertOne(category)
      .then((category: Category) => {
        response = res.status(200).send({ status: 'OK', data: category });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * * Edit a Category
   *
   * * Call categoryService findOneAndUpdate to edit a category
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Category} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  edit = async (req: Request, res: Response) => {
    let response: Response;
    let category = req.body;
    category.id = req.params.id;

    this.categoryService
      .findOneAndUpdate(category)
      .then((category: Category) => {
        response = res.status(200).send({
          status: 'OK',
          data: category,
        });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * * Delete a category
   *
   * * Call categoryService findOneAndDelete to delete a Category
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', message: string} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  delete = async (req: Request, res: Response) => {
    let response: Response;
    const id: string = req.params.id;

    this.categoryService
      .findOneAndDelete(id)
      .then(() => {
        response = res
          .status(200)
          .send({ status: 'OK', message: 'Category deleted With Sucess' });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };
}

export default CategoryController;
