import { Request, Response } from 'express';
// Sevices
import TagService from '../services/tag.service';
// Entities
import { Tag } from '../entities/tag.entity';

/**
 * * The tag controller
 */
class TagController {
  tagService: TagService;

  /**
   * * The TagController
   *
   * * Inject Category service
   */
  constructor() {
    this.tagService = new TagService();
  }

  /**
   * * Get All Tags
   *
   * * Call TagService > find to get all tags
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status:'OK', data:Tags[]} | 400 on error {status: 'ERROR}, error : {name:string, message:string}}
   *
   */
  get = async (req: Request, res: Response) => {
    let response: Response;
    this.tagService
      .find(Tag.getColumns())
      .then((tag: Tag[]) => {
        response = res
          .status(200)
          .send({ status: 'OK', data: tag, count: tag.length });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * * Get a tag
   *
   * * Call TagService > findOne to get a Tag
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} return: 200 on success {status:'OK', data:tag} | 400 on error {status:'ERROR', error: {name:string, message:string }}
   */
  getOneById = async (req: Request, res: Response) => {
    let response: Response;
    const id: string = req.params.id;
    //
    this.tagService
      .findOne(id, Tag.getColumns())
      .then((tag: Tag) => {
        response = res.status(200).send({ status: 'OK', data: tag });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * * Create an Tag
   *
   * * Call TagService insertOne to create a Tag
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Tag} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  create = async (req: Request, res: Response) => {
    let response: Response;
    let tag: Tag = req.body;

    this.tagService
      .insertOne(tag)
      .then((tag: Tag) => {
        response = res.status(200).send({ status: 'OK', data: tag });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * * Edit a Tag
   *
   * * Call tagService findOneAndUpdate to edit a Tag
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Tag} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  edit = async (req: Request, res: Response) => {
    let response: Response;
    let tag = req.body;
    tag.id = req.params.id;

    this.tagService
      .findOneAndUpdate(tag)
      .then((tag: Tag) => {
        response = res.status(200).send({
          status: 'OK',
          data: tag,
        });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * * Delete a Tag
   *
   * * Call TagService findOneAndDelete to delete a Tag
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', message: string} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  delete = async (req: Request, res: Response) => {
    let response: Response;
    const id: string = req.params.id;

    this.tagService
      .findOneAndDelete(id)
      .then(() => {
        response = res
          .status(200)
          .send({ status: 'OK', message: 'Tag deleted with Sucess' });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };
}

export default TagController;
