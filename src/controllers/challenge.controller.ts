import { Request, Response } from 'express';
// Services
import ChallengeService from '../services/challenge.service';
// Entities
import { Challenge } from '../entities/challenge.entity';

/**
 * The challenge controller
 */
class ChallengeController {
  challengeService: ChallengeService;

  /**
   * The challenge constructor
   *
   * Inject Challenge service
   */
  constructor() {
    this.challengeService = new ChallengeService();
  }

  /**
   * Get all challenges
   *
   * Call ChallengeService find to get all challenges
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Challenge[]} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  get = (req: Request, res: Response) => {
    let response: Response;
    this.challengeService
      .find(Challenge.getColumns())
      .then((challenges: Challenge[]) => {
        response = res
          .status(200)
          .send({ status: 'OK', data: challenges, count: challenges.length });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * Get an challenge
   *
   * Call ChallengeService findOne to get an challenge
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Challenge} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  getOneById = (req: Request, res: Response): Response => {
    let response: Response;
    const id: string = req.params.id;

    this.challengeService
      .findOne(id, Challenge.getColumns())
      .then((challenge: Challenge) => {
        response = res.status(200).send({ status: 'OK', data: challenge });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * Create an challenge
   *
   * Call ChallengeService insertOne to create an challenge
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Challenge} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  create = (req: Request, res: Response): Response => {
    let response: Response;
    let challenge: Challenge = req.body;

    this.challengeService
      .insertOne(challenge)
      .then((challenge: Challenge) => {
        response = res.status(200).send({ status: 'OK', data: challenge });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * Edit an challenge
   *
   * Call ChallengeService findOneAndUpdate to edit an challenge
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Challenge} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  edit = (req: Request, res: Response): Response => {
    let response: Response;
    let challenge = req.body;
    challenge.id = req.params.id;

    this.challengeService
      .findOneAndUpdate(challenge)
      .then((challenge: Challenge) => {
        response = res.status(200).send({
          status: 'OK',
          data: challenge,
        });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * Delete an challenge
   *
   * Call ChallengeService findOneAndDelete to delete an challenge
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', message: string} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  delete = (req: Request, res: Response): Response => {
    let response: Response;
    const id: string = req.params.id;

    this.challengeService
      .findOneAndDelete(id)
      .then(() => {
        response = res
          .status(200)
          .send({ status: 'OK', message: 'Challenge deleted With Sucess' });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };
}

// Export ChallengeController
export default ChallengeController;
