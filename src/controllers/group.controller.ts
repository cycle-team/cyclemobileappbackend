import { Request, Response } from 'express';
// Services
import GroupService from '../services/group.service';
// Entities
import { Group } from '../entities/group.entity';

/**
 * The group controller
 */
class GroupController {
  /**
   * The group service
   */
  groupService: GroupService;

  constructor() {
    // Inject group service
    this.groupService = new GroupService();
  }

  get = async (req: Request, res: Response) => {
    let groups = [];

    try {
      groups = await this.groupService.find(Group.getColumns());
    } catch (error) {
      return res.status(404).send('Groups not found');
    }

    //Send the groups object
    return res
      .status(200)
      .send({ status: 'OK', data: groups, count: groups.length });
  };

  getOneById = async (req: Request, res: Response) => {
    // Get the ID from the url
    const id: string = req.params.id;
    let group: Group;

    // try get group from service
    try {
      group = await this.groupService.findOne(id, Group.getColumns());
    } catch (error) {
      return res.status(404).send('Group not found');
    }

    return res.status(200).send({ status: 'OK', data: group });
  };

  create = async (req: Request, res: Response) => {
    let group: Group = req.body;

    try {
      group = await this.groupService.insertOne(group);
    } catch (error) {
      return res.status(404).send('Group not found');
    }

    return res.status(200).send({ status: 'OK', data: group });
  };

  edit = async (req: Request, res: Response) => {
    let group = req.body;
    group.id = req.params.id;

    // try edit group from service
    try {
      group = await this.groupService.findOneAndUpdate(group);
    } catch (e) {
      res.status(409).send('group already exist');
      return;
    }

    return res.status(200).send({ status: 'OK', data: group });
  };

  remove = async (req: Request, res: Response) => {
    // get the ID from request params
    const id: string = req.params.id;

    // try delete group from service
    try {
      await this.groupService.findOneAndDelete(id);
    } catch {
      return res.status(404).send('Group not found');
    }

    return res
      .status(200)
      .send({ status: 'OK', message: 'Group succefully deleted' });
  };
}

export default GroupController;
