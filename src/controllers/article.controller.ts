import { Request, Response, response } from 'express';
// Services
import ArticleService from '../services/article.service';
// Entities
import { Article } from '../entities/article.entity';

/**
 * The article controller
 */
class ArticleController {
  /**
   * The article service
   */
  articleService: ArticleService;

  /**
   * The article constructor
   *
   * Inject Article service
   */
  constructor() {
    this.articleService = new ArticleService();
  }

  /**
   * Get all articles
   *
   * Call ArticleService find to get all challenges
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Article[]} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  get = (req: Request, res: Response): Response => {
    let response: Response;
    this.articleService
      .find(Article.getColumns())
      .then((articles: Article[]) => {
        response = res
          .status(200)
          .send({ status: 'OK', data: articles, count: articles.length });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * Get an article
   *
   * Call ArticleService findOne to get an article
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Article} | 400 on error {}
   */
  getOneById = (req: Request, res: Response): Response => {
    let response: Response;
    const id: string = req.params.id;

    this.articleService
      .findOne(id, Article.getColumns())
      .then((article: Article) => {
        response = res.status(200).send({ status: 'OK', data: article });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * Create an article
   *
   * Call ArticleService insertOne to create an article
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: article} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   * */
  create = (req: Request, res: Response): Response => {
    let response: Response;
    let article: Article = req.body;

    this.articleService
      .insertOne(article)
      .then((article: Article) => {
        response = res.status(200).send({ status: 'OK', data: article });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * Edit an article
   *
   * Call ArticleService findOneAndUpdate to edit an article
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', data: Article} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  edit = (req: Request, res: Response): Response => {
    let response: Response;
    let article = req.body;
    article.id = req.params.id;

    this.articleService
      .findOneAndUpdate(article)
      .then((article: Article) => {
        response = res.status(200).send({
          status: 'OK',
          data: article,
        });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };

  /**
   * Delete an article
   *
   * Call ArticleService findOneAndDelete to delete an article
   *
   * @param {Request} req The request object
   * @param {Response} res The response object
   *
   * @returns {Response} Return: 200 on success {status: 'OK', message: string} | 400 on error {status: 'ERROR', error: {name: string, message: string}}
   */
  remove = (req: Request, res: Response): Response => {
    let response: Response;
    const id: string = req.params.id;

    this.articleService
      .findOneAndDelete(id)
      .then(() => {
        response = res
          .status(200)
          .send({ status: 'OK', message: 'Article deleted with Success' });
      })
      .catch((error) => {
        response = res.status(400).send({
          status: 'ERROR',
          error: { name: error.name, message: error.message },
        });
      });

    return response;
  };
}

// Export ArticleController
export default ArticleController;
