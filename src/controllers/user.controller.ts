import { Request, Response } from 'express';
// Services
import UserService from '../services/user.service';
// Entities
import { User } from '../entities/user.entity';
// Models
import { UserModel } from '../models/user.model';

/**
 * The user controller
 */
class UserController {
  /**
   * The user service
   */
  userService: UserService;

  constructor() {
    // Inject user service
    this.userService = new UserService();
  }

  /**
   * Get all users
   *
   * @param req The request object
   * @param res The response object
   */
  get = async (req: Request, res: Response) => {
    let users: User[];

    // try get users from service
    try {
      users = await this.userService.find(User.getColumns());
    } catch {
      return res.status(404).send('User not found');
    }

    return res
      .status(200)
      .send({ status: 'OK', data: users, count: users.length });
  };

  /**
   * Get an user by id
   *
   * @param req The request object
   * @param res The response object
   */
  getOneById = async (req: Request, res: Response) => {
    //Get the ID from params
    const id: string = req.params.id;
    let user: User;

    // try get user from service
    try {
      user = await this.userService.findOne(id, User.getColumns());
    } catch (error) {
      return res.status(404).send('User not found');
    }

    return res.status(200).send({ status: 'OK', data: user });
  };

  /**
   * Create an user
   *
   * @param req The request object
   * @param res The response object
   */
  create = async (req: Request, res: Response) => {
    // TODO verify unique username

    //Get the ID from the url
    let user: UserModel | User = req.body;

    // try create user from service
    try {
      user = await this.userService.insertOne(user);
    } catch (error) {
      return res.status(404).send('User not found');
    }

    return res.status(200).send({ status: 'OK', data: user });
  };

  /**
   * Edit an user by id
   *
   * @param req The request object
   * @param res The response object
   */
  edit = async (req: Request, res: Response) => {
    let user = req.body;
    user.id = req.params.id;

    // try edit user from service
    try {
      user = await this.userService.findOneAndUpdate(user);
    } catch (e) {
      res.status(409).send('username already in use');
      return;
    }

    return res.status(200).send({ status: 'OK', data: user });
  };

  /**
   * Delete an user by id
   *
   * @param req The request object
   * @param res The response object
   */
  delete = async (req: Request, res: Response) => {
    // get the ID from request params
    const id: string = req.params.id;

    // try delete user from service
    try {
      await this.userService.findOneAndDelete(id);
    } catch {
      return res.status(404).send('User not found');
    }

    return res
      .status(200)
      .send({ status: 'OK', message: 'User succefully deleted' });
  };
}

export default UserController;
