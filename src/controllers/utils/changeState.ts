import { getConnection } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export async function changeState(table, newModState: QueryDeepPartialEntity<any>, id: number) {
  await getConnection()
    .createQueryBuilder()
    .update(table)
    .set({
      modState: newModState,
    })
    .where('id=:id', { id: id })
    .execute();
}
