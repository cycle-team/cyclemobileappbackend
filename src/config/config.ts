const config = require('../../utils.js').getConfigFile();

export default {
  ...config,
  jwtSecret: '@HPTMOBILECYCLEAPPSECRET',
};
