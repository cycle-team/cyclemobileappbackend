import { UserModel } from './user.model';
import { TagModel } from './tag.model';
import { CategoryModel } from './category.model';

export class ArticleModel {
  id?: string;
  title?: string;
  description?: string;
  content?: string;
  image?: string;
  category?: CategoryModel;
  like?: number;
  views?: number;
  published?: boolean;
  user?: UserModel;
  tags?: TagModel[];
}
