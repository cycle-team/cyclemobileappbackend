import { UserModel } from "./user.model";

export class GroupModel {
  id?: string;
  name: string;
  description: string;
  image: string;
  isActive: boolean;
  users: UserModel[];
}
