import { ArticleModel } from "./article.model";
import { GroupModel } from './group.model';
import { ChallengeModel } from './challenge.model';

export class UserModel {
  id?: string;
  gender?: string;
  birthday?: string;
  age?: Number;
  username?: string;
  password?: string;
  role?: string;
  groups?: GroupModel[];
  articles?: ArticleModel[];
  challenges?: ChallengeModel[];
}
