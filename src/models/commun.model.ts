import { MODERATIONSTATE } from "../utils/moderationState.enum";

export abstract class CommunModel {
  id?: string;
  lang: string;
  createAt;
  updateAt?;
  title: string;
  listImage: string[];
  content: {}[];
  modState: MODERATIONSTATE;
  isActive: boolean;
  isDelete: boolean;
}
