export class TagModel {
  id?: string;
  lang?: string;
  name?: string;
  count?: number;
  popularity?: number;
  createAt?;
  challenges?: [];
}
