import { CategoryModel } from './category.model';
import { UserModel } from './user.model';
import { difficultyChallenge } from '../utils/challenge/difficultyChallenge.enum';
import { challengeFrequency } from '../utils/challenge/frequencyChallenge.enum';
import { CommunModel } from './commun.model';
import { TagModel } from './tag.model';

export class ChallengeModel extends CommunModel {
  intro: string;
  desc: string;
  difficulty: difficultyChallenge;
  frequency: challengeFrequency;
  coverImg: string;
  sources: string[];
  authorChallenge: UserModel;
  tags: TagModel[];
  category: CategoryModel;
}

class TagToCreate {
  id: string;
}
