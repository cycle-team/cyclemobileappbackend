import { createConnection, getConnection } from 'typeorm';
// repositories
import { UserRepository } from './repositories/user.repository';
import { CategoryRepository } from './repositories/category.repository';
import { ArticleRepository } from './repositories/article.repository';
import { GroupRepository } from './repositories/group.repository';
import { ChallengeRepository } from './repositories/challenge.repository';
import { TagRepository } from './repositories/tag.repository';

export const createDatabaseConnexion = async () => {
  return createConnection();
};

export const closeDatabaseConnection = async () => {
  return getConnection().close();
};

/**
 * Set all repositories
 */
export const setRepositories = (connection) => {
  UserRepository.getInstance(connection);
  ArticleRepository.getInstance(connection);
  GroupRepository.getInstance(connection);
  CategoryRepository.getInstance(connection);
  ChallengeRepository.getInstance(connection);
  TagRepository.getInstance(connection);
};
