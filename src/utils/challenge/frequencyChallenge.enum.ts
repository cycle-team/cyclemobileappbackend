export enum challengeFrequency {
    DAY = 'Dayli',
    WEEK = 'Weekly',
    MONTH = 'Monthly',
    PUNCTUAL = 'Punctual',
    RECURRENT = 'Recurrent'
}