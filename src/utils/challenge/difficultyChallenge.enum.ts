export enum difficultyChallenge {
    LOW = "Low",
    MIDDLE = "Middle",
    HIGH = "High"
}