export enum MODERATIONSTATE {
    DRAFT = 'DRAFT', // Brouillon
    DONEWAITQ = 'DONE & WAIT Q', // Fini en attente de modération
    DONEWAITMOD = 'DONE & WAIT MOD', // Fini en cours de modération
    VALIDATE = "VALIDATE", // Après modération - Contenu validé par la communauté
    REFUSE = "REFUSE", // Après modération - Contenu Refuser par la communauté
    CANCEL = "CANCEL", // Annulé - Supprimer par l'autheur du contenu
    REJECT = "REJECT" // Exeptionnel - Rejeter par l'application
}
