import * as request from 'supertest';
import {
  articleMock,
  articleToCreateMock,
  ARTICLE_MOCK_ID,
  articleToEditMock,
} from '../../src/mocks/article.mock';
import { getTestToken } from '../../jest.setup';
import app from '../../src/app';

describe('ArticleController', () => {
  describe('get', () => {
    it('isAuth =true | return 200 & []', (done) => {
      request(app.getApp())
        .get('/articles')
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/articles')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('getOneById', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .get(`/articles/${articleMock.id}`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/articles')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });

    it('isAuth = true | article not exist | return 400 & {}', (done) => {
      request(app.getApp())
        .get(`/articles/idnotexist`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(400);
          expect(response.body).toMatchObject({
            status: 'ERROR',
            error: {
              name: 'EntityNotFound',
            },
          });
          done();
        });
    });
  });

  describe('create', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .post('/articles')
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .send(articleToCreateMock)
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            data: articleToCreateMock,
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .post('/articles')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('edit', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .patch(`/articles/${ARTICLE_MOCK_ID.EDIT}`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .send(articleToEditMock)
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            data: expect.objectContaining({
              title: 'Article édité',
              category: {
                id: '5b7733be-3450-47e7-8fa5-3b4e098c573',
                lang: 'fr',
                name: 'Gestion des déchets',
              },
            }),
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .patch(`/articles/${ARTICLE_MOCK_ID.EDIT}`)
        .set('Accept', 'application/json')
        .send(articleToEditMock)
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });

    it('isAuth = true | article not exist | return 400 & {}', (done) => {
      request(app.getApp())
        .patch(`/articles/idnotexist`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .send(articleToEditMock)
        .then((response) => {
          expect(response.status).toBe(400);
          done();
        });
    });
  });

  describe('delete', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .delete(`/articles/${ARTICLE_MOCK_ID.DELETE}`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            message: 'Article deleted with Success',
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .delete(`/articles/${ARTICLE_MOCK_ID.DELETE}`)
        .set('Accept', 'application/json')
        .send(articleToEditMock)
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });

    it('isAuth = true | article not exist | return 400 & {}', (done) => {
      request(app.getApp())
        .delete(`/articles/idnotexist`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(400);
          expect(response.body).toMatchObject({
            status: 'ERROR',
            error: {
              name: 'NotExist',
              message: 'Article with id idnotexist not found.',
            },
          });
          done();
        });
    });
  });
});
