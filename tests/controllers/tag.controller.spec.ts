//Persoonal import
import app from '../../src/app';
import { getTestToken } from '../../jest.setup';
// Lib
import * as request from 'supertest';
import { tagToCreateMock } from '../../src/mocks/tag.mock';

/**
 *
 * * TagController Test
 *
 * All be test.
 * ? create
 *  - isAuth = true | return 200 & {}
 *  - isAuth = false | return 401
 * ? get
 *  - isAuth = true | return 200 & []
 *  - isAuth = true | id = false | return 200 & {}
 * ? getOneById
 *  - isAuth = true | id = true | return 200 & {}
 *  - isAuth = true | id = false | return 200 & {}
 *  - isAuth = false | return 401
 * ? delete
 *  - isAuth = true | id = true | return 200 & {}
 *  - isAuth = true | id = false | return 400
 *  - isAuth = false | return 401
 *
 */
describe('TagController', () => {
  describe('create', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .post(`/tags`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .send(tagToCreateMock)
        .then((response) => {
          delete response.body.data.id;
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            data: expect.objectContaining({
              name: 'TagCreatedByJest',
            }),
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/tags')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('get', () => {
    it('isAuth = true | return 200 & []', (done) => {
      request(app.getApp())
        .get('/tags')
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/tags')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  //TODO: test voir avec les tableaux le comportement.
  describe('getOneById', () => {
    it('isAuth = true | id = true | return 200 & {}', (done) => {
      request(app.getApp())
        .get(`/tags/30cc0ca2-e3b4-4b3f-8e41-b28if34df8bf`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            data: expect.objectContaining({
              name: 'KiffTesDechets',
            }),
          });
          done();
        });
    });

    it('isAuth = true | id = false | return 200 & {}', (done) => {
      request(app.getApp())
        .get(`/tags/1234`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          delete response.body.error.message;
          expect(response.status).toBe(400);
          expect(response.body).toMatchObject({
            status: 'ERROR',
            error: {
              name: 'EntityNotFound',
            },
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/tags')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('delete', () => {
    it('isAuth = true | id = true | return 200 & {}', (done) => {
      request(app.getApp())
        .delete(`/tags/TagCreateByJestTest`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            message: 'Tag deleted with Sucess',
          });
          done();
        });
    });

    it('isAuth = true | id = false | return 400', (done) => {
      request(app.getApp())
        .delete(`/tags/1234`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          //delete response.body.error.message;
          expect(response.status).toBe(400);
          expect(response.body).toMatchObject({
            status: 'ERROR',
            error: {
              name: 'NotExist',
              message: expect.any(String),
            },
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .delete(`/categories/30cc0ca2-e3b4-4b3f-8e41-b28if34df8bf`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });
});
