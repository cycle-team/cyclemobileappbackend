import {
  categoryMock,
  categoryToCreateMock,
  categoryEdit,
} from './../../src/mocks/category.mock';
import * as request from 'supertest';
import { getTestToken } from '../../jest.setup';
import app from '../../src/app';

/**
 *
 * * CategoryController Test
 *
 * All be test.
 * ? create
 *  - isAuth = true | return 200 & {}
 *  - isAuth = false | return 401
 * ? get
 *  - isAuth = true | return 200 & []
 *  - isAuth = true | id = false | return 200 & {}
 * ? getOneById
 *  - isAuth = true | id = true | return 200 & {}
 *  - isAuth = true | id = false | return 400 & {}
 *  - isAuth = false | return 401
 * ? delete
 *  - isAuth = true | id = true | return 200 & {}
 *  - isAuth = true | id = false | return 400
 *  - isAuth = false | return 401
 * ? edit
 *  - isAuth = true | id = true | return 200 & {}
 *  - isAuth = true | id = false | return 400 & {}
 *  - isAuth = false | return 401
 */
describe('CategoryController', () => {
  describe('create', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .post(`/categories`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .send(categoryToCreateMock)
        .then((response) => {
          delete response.body.data.id;
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            data: expect.any(Object),
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/categories')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('get', () => {
    it('isAuth = true | return 200 & []', (done) => {
      request(app.getApp())
        .get('/categories')
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/categories')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  //TODO: test voir avec les tableaux le comportement.
  describe('getOneById', () => {
    it('isAuth = true | id = true | return 200 & {}', (done) => {
      request(app.getApp())
        .get(`/categories/5b2433be-3850-47e7-8fa5-3b4e098c574`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            data: expect.objectContaining({
              lang: expect.any(String),
              name: expect.any(String),
            }),
          });
          done();
        });
    });

    it('isAuth = true | id = false | return 400 & {}', (done) => {
      request(app.getApp())
        .get(`/categories/1234`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          delete response.body.error.message;
          expect(response.status).toBe(400);
          expect(response.body).toMatchObject({
            status: 'ERROR',
            error: {
              name: 'EntityNotFound',
            },
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/categories')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('edit', () => {
    it('isAuth = true | id = true | return 200 & {}', (done) => {
      request(app.getApp())
        .patch(`/categories/CategoryCreatedByJest`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .send(categoryEdit)
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            data: expect.objectContaining({
              lang: 'fr',
              name: 'Category created by Jest & Modify',
            }),
          });
          done();
        });
    });

    it('isAuth = true | id = false | return 400 & {}', (done) => {
      request(app.getApp())
        .patch(`/categories/CategoryCreatedByJestlol`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .send(categoryEdit)
        .then((response) => {
          expect(response.status).toBe(400);
          expect(response.body).toMatchObject({
            status: 'ERROR',
            error: expect.objectContaining({
              name: 'EntityNotFound',
              message: expect.any(String),
            }),
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .patch('/categories/CategoryCreatedByJest')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('delete', () => {
    it('isAuth = true | id = true | return 200 & {}', (done) => {
      request(app.getApp())
        .delete(`/categories/CategoryCreatedByJest`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            message: 'Category deleted With Sucess',
          });
          done();
        });
    });

    it('isAuth = true | id = false | return 400', (done) => {
      request(app.getApp())
        .delete(`/categories/1234`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          //delete response.body.error.message;
          expect(response.status).toBe(400);
          expect(response.body).toMatchObject({
            status: 'ERROR',
            error: {
              name: 'NotExist',
              message: expect.any(String),
            },
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .delete(`/categories/${categoryMock.id}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });
});
