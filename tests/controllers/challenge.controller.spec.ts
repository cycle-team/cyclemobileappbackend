import * as request from 'supertest';
import { getTestToken } from '../../jest.setup';
import app from '../../src/app';

describe('ChallengeController', () => {
  describe('get', () => {
    it('isAuth = true | return 200 & []', (done) => {
      request(app.getApp())
        .get('/challenges')
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/challenges')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('getOneById', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .get('/challenges')
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/challenges')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });

    it('isAuth = true | challenge not exist | return 400 & {}', (done) => {
      request(app.getApp())
        .get(`/challenges/idnotexist`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          delete response.body.error.message;
          expect(response.status).toBe(400);
          expect(response.body).toMatchObject({
            status: 'ERROR',
            error: {
              name: 'EntityNotFound',
            },
          });
          done();
        });
    });
  });
});
