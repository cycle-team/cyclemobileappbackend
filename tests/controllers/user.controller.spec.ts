import * as request from 'supertest';
import {
  userMock,
  userToCreateMock,
  usersMock,
} from '../../src/mocks/user.mock';
import { getTestToken } from '../../jest.setup';
import app from '../../src/app';

describe('UserController', () => {
  describe('get', () => {
    it('isAuth = true | return 200 & []', (done) => {
      request(app.getApp())
        .get('/users')
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/users')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });

  describe('getOneById', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .get(`/users/${userMock.id}`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/users')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });

    it('isAuth = true | user not exist | return 200 & {}', (done) => {
      request(app.getApp())
        .get(`/users/${userMock.id}`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(200);
          done();
        });
    });
  });

  describe('create', () => {
    it('isAuth = true | return 200 & {}', (done) => {
      request(app.getApp())
        .post(`/users`)
        .set('Authorization', `Bearer ${getTestToken()}`)
        .set('Accept', 'application/json')
        .send(userToCreateMock)
        .then((response) => {
          delete response.body.data.password;
          delete response.body.data.id;
          delete userToCreateMock.password;
          expect(response.status).toBe(200);
          expect(response.body).toMatchObject({
            status: 'OK',
            data: userToCreateMock,
          });
          done();
        });
    });

    it('isAuth = false | return 401', (done) => {
      request(app.getApp())
        .get('/users')
        .set('Accept', 'application/json')
        .then((response) => {
          expect(response.status).toBe(401);
          done();
        });
    });
  });
});
