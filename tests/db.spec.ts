import { createDatabaseConnexion, closeDatabaseConnection } from '../src/db';
import { getConnection } from 'typeorm';
import { promises } from 'dns';

describe('Database', () => {
  it('createDatabaseConnexion', () => {
    if (!getConnection().isConnected) {
      createDatabaseConnexion().then((connection) => {
        expect(connection.isConnected).toBe(true);
      });
    }
  });

  it('closeDatabaseConnection', async () => {
    await closeDatabaseConnection();
    expect(getConnection().isConnected).toBe(false);
  });
});
