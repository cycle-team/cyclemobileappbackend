const config = require('./utils').getConfigFile();
const envIsDev = require('./utils').envIsDev();

module.exports = [
  {
    environment: config.bdd.env,
    name: config.bdd.name,
    type: config.bdd.type,
    host: config.bdd.host,
    port: config.bdd.port,
    username: config.bdd.username,
    password: config.bdd.password,
    database: config.bdd.database,
    synchronize: true,
    autoLoadEntities: true,
    autoSchemaSync: true,
    logging: false,
    entities: [
      `${envIsDev ? './src/entities/**/*.ts' : './dist/entities/**/*.js'}`,
    ],
    migrations: [
      `${envIsDev ? './src/migrations/**/*.ts' : './dist/migrations/**/*.js'}`,
    ],
    subscribers: [`src/subscriber/**/*.${envIsDev ? 'ts' : 'js'}`],
    cli: {
      entitiesDir: 'src/entities',
      migrationsDir: 'src/migrations',
      subscribersDir: 'src/subscriber',
    },
    connectTimeout: 3000,
  },
];
