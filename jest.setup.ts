import { createDatabaseConnexion, setRepositories } from './src/db';
import { getConnection } from 'typeorm';
import * as request from 'supertest';
import app from './src/app';
const utils = require('./utils');

let token;

export const getTestToken = () => {
  return token;
};

beforeEach(async (done) => {
  jest.setTimeout(30000);
  if (!getConnection().isConnected) {
    createDatabaseConnexion()
      .then(async (connection) => {
        setRepositories(connection);
        await reloadFixtures();
        token = await setTestToken(done);
        done();
      })
      .catch((error) => {
        console.log(error);
      });
  } else {
    setRepositories(getConnection());
    token = await setTestToken(done);
  }
});

afterEach(async () => {
  if (app.server) {
    await shutdownServer(app.server);
  }
});

export const setTestToken = (done) => {
  return new Promise((resolve, reject) => {
    request(app.getApp())
      .post('/auth/login')
      .send({ username: 'admin', password: 'admin' })
      .set('Accept', 'application/json')
      .expect(200)
      .then((res) => {
        resolve(res.body.token);
        done();
      })
      .catch((error) => reject(error));
  });
};

const shutdownServer = async (server) => {
  await server.httpServer.close();
  await closeDbConnection();
};

const closeDbConnection = async () => {
  if (getConnection().isConnected) {
    getConnection().close();
  }
};

const reloadFixtures = async () => {
  await cleanAll();
  await loadAll();
};

const cleanAll = async () => {
  await utils.execute('npm run db:drop:test');
  await utils.execute('npm run db:create:test');
  await utils.execute('npm run db:sync:test');
};

const loadAll = async () => {
  return utils.execute('npm run db:migration:run:test');
};
