const config = getConfigFile();

module.exports.createDb = function () {
  execute(
    `mysql -u${config.bdd.username} ${
      config.bdd.password ? '-p' + config.bdd.password : ''
    } -e "CREATE DATABASE ${config.bdd.database}"`
  )
    .then(() => {
      console.log('====================================');
      console.log('===== DATABASE CREATED SUCCESS =====');
      console.log('====================================');
    })
    .catch((error) => {
      console.log('====================================');
      console.log('====== DATABASE CRETED FAILED ======');
      console.log('====================================');
    });
};

module.exports.dropDb = function () {
  execute(
    `mysql -u${config.bdd.username} -h${config.bdd.host} ${
      config.bdd.password ? '-p' + config.bdd.password : ''
    } -e "DROP DATABASE ${config.bdd.database}"`
  )
    .then(() => {
      console.log('====================================');
      console.log('====== DATABASE DROP SUCCESS =======');
      console.log('====================================');
    })
    .catch((error) => {
      console.log('====================================');
      console.log('======= DATABASE DROP FAILED =======');
      console.log('====================================');
    });
};

module.exports.dropDbProd = function () {};

/**
 * Execute postbuild heroku command to clean server workspace
 */
module.exports.postbuild = async function () {
  await execute(`cp -r ${__dirname}/dist/src/* ${__dirname}/dist`);
  await execute(`rm -rf  ${__dirname}/dist/src`);
  await execute(`rm -rf  ${__dirname}/src`);
  await execute(`rm -rf  ${__dirname}/assets`);
};

/**
 * Execute shell|bash commands from node child_process
 *
 * @param command The shell command to execute
 */
async function execute(command) {
  const exec = require('child_process').exec;

  return new Promise((resolve, eject) => {
    exec(command, (err, stdout, stderr) => {
      if (err) {
        eject(process.stdout.write(stderr));
      } else {
        resolve(process.stdout.write(stdout));
      }
    });
  });
}

function getConfigFile() {
  switch (process.env.NODE_ENV) {
    case 'production':
      return require('./config/config');
      break;
    case 'development':
      return require('./config/local-config');
      break;
    case 'test':
      return require('./config/test-config');
      break;
  }
}

module.exports.getConfigFile = getConfigFile;

function envIsDev() {
  return process.env.NODE_ENV === 'development' || 'test' ? true : false;
}

module.exports.envIsDev = envIsDev;

module.exports.execute = execute;
